## Description

Currently at Deevotech, we're using React and mostly VueJS throughout our projects. 
We don't want to ask you abstract technical questions during the interview, so we have a small exercise for you.

The exercise is to build a front-end application using HTML/CSS and any language/framework. You are free to use VueJS or ReactJS with any stack you prefer (create-react-app, vue-cli, vite, etc.) and we will also evaluate your knowledge on the technologies that you selected.

## Requirements

Using [TheCocktailDB](https://www.thecocktaildb.com/api.php) , write a program that can search for a cocktail by name and display the list of results.
For example, searching for "margarita" should return a list of 6 results with their images and description in available languages. 

## Tips

- The design is up to you, surprise us 😛!
- Tests are always welcome.
